package com.azermehdibeyli.www.a4daysschedule;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by azer.mehdibeyli on 27.11.2017.
 */

public class Settings {
    Context context;

    public Settings(Context context) {
        this.context = context;
    }

    public int getShiftNoFromSettings() {
        int ret = 0;

        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preferenceFileName), Context.MODE_PRIVATE);

        try {
            ret = sharedPref.getInt(context.getString(R.string.scheduleShiftNo), 0);
        } catch (Exception e) {
        }

        return ret;
    }

    public Date getFirstWorkingDateFromSettings(int shiftNo) {
        Date date;
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preferenceFileName), Context.MODE_PRIVATE);
        Calendar cal = Calendar.getInstance();
        try {
            String dateStringFromPrefence = sharedPref.getString(context.getString(R.string.scheduleStartDate) + shiftNo, null);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(dateStringFromPrefence);
        } catch (Exception e) {
            date = cal.getTime();
        }

        return date;
    }

    public boolean isAllDatesAdjusted() {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preferenceFileName), Context.MODE_PRIVATE);
        boolean ret = true;
        for (int i = 0; i < 4; i++) {
            try {
                String dateStringFromPrefence = sharedPref.getString(context.getString(R.string.scheduleStartDate) + i, null);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                sdf.parse(dateStringFromPrefence);
            } catch (Exception e) {
                ret = false;
                break;
            }
        }
        return ret;
    }

    public boolean isSwipeTrained() {
        boolean ret = true;
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preferenceFileName), Context.MODE_PRIVATE);
        try {
            ret = sharedPref.getBoolean(context.getString(R.string.isSwipeTrained), false);
        } catch (Exception e) {
        }
        return ret;
    }

    public void setSwipeTrained(boolean trained) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                context.getString(R.string.preferenceFileName), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(context.getString(R.string.isSwipeTrained), trained);
        editor.commit();
    }
}
