package com.azermehdibeyli.www.a4daysschedule;

import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by Azer on 09.12.2017.
 */

class CustomGridViewGestureListener extends GestureDetector.SimpleOnGestureListener {

    private static final int SWIPE_THRESHOLD = 85;
    private static final int SWIPE_VELOCITY_THRESHOLD = 99;

    @Override
    public boolean onDown(MotionEvent event) {
        Log.i("onDown", "onDown");

        return true;
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2,
                           float velocityX, float velocityY) {

        float diffY = event2.getY() - event1.getY();
        float diffX = event2.getX() - event1.getX();
        if (Math.abs(diffX) > Math.abs(diffY)) {
            if (Math.abs(diffX) > SWIPE_THRESHOLD || Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (diffX > 0) {
                    onSwipeRight();
                } else {
                    onSwipeLeft();
                }
            }
        } else {
            if (Math.abs(diffY) > SWIPE_THRESHOLD || Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                if (diffY > 0) {
                    onSwipeBottom();
                } else {
                    onSwipeTop();
                }
            }
        }
        return true;
    }

    public void onSwipeLeft() {
        Log.i("onSwipeLeft", "onSwipeLeft");
    }

    public void onSwipeRight() {
        Log.i("onSwipeRight", "onSwipeRight");

    }

    public void onSwipeTop() {
        Log.i("onSwipeTop", "onSwipeTop");

    }

    public void onSwipeBottom() {
        Log.i("onSwipeBottom", "onSwipeBottom");

    }
}
