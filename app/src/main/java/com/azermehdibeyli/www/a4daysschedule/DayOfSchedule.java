package com.azermehdibeyli.www.a4daysschedule;

/**
 * Created by Azer on 26.11.2017.
 */

public class DayOfSchedule {
    private DayType dayType;
    private int day;

    public DayOfSchedule(DayType dayType, int day) {
        this.setDayType(dayType);
        this.setDay(day);
    }

    public DayOfSchedule() {
        this.setDayType(DayType.DEFAULT);
        this.setDay(0);
    }


    public DayType getDayType() {
        return dayType;
    }

    public void setDayType(DayType dayType) {
        this.dayType = dayType;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
