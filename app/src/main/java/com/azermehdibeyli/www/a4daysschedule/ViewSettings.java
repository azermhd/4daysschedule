package com.azermehdibeyli.www.a4daysschedule;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by azer.mehdibeyli on 28.12.2017.
 */

public class ViewSettings {
    private int screenX;
    private int screenY;

    private int mainGridH;
    private int mainGridW;

    private int secondGridH;
    private int secondGridW;

    private int gridCellH;
    private int gridCellW;

    public ViewSettings(Activity activity){
        float density = 1f;
        int gridCellPadding = 2;
        int gridCellWidth = 100;
        int gridCellImageWidth = 100;
        int gridWidth = 500;
        int screenWidth = 720;
        int main_mainParentLayoutPadding;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.widthPixels;
        density = displayMetrics.density;

        main_mainParentLayoutPadding = Math.round(density * 10);

        gridWidth = screenWidth - main_mainParentLayoutPadding * 2;
        gridCellWidth = (int) ((float) gridWidth / 7f);
        gridCellPadding = (int) (density * 1f) + 1;
        gridCellImageWidth = gridCellWidth - 2 * gridCellPadding;

        gridCellW = gridCellImageWidth;
        gridCellH = gridCellImageWidth;

        mainGridH = gridWidth;
        mainGridW = gridWidth;

        secondGridH = gridWidth;
        secondGridW = gridWidth;
    }

    public int getScreenX() {
        return screenX;
    }

    public void setScreenX(int screenX) {
        this.screenX = screenX;
    }

    public int getScreenY() {
        return screenY;
    }

    public void setScreenY(int screenY) {
        this.screenY = screenY;
    }

    public int getMainGridH() {
        return mainGridH;
    }

    public void setMainGridH(int mainGridH) {
        this.mainGridH = mainGridH;
    }

    public int getMainGridW() {
        return mainGridW;
    }

    public void setMainGridW(int mainGridW) {
        this.mainGridW = mainGridW;
    }

    public int getSecondGridH() {
        return secondGridH;
    }

    public void setSecondGridH(int secondGridH) {
        this.secondGridH = secondGridH;
    }

    public int getSecondGridW() {
        return secondGridW;
    }

    public void setSecondGridW(int secondGridW) {
        this.secondGridW = secondGridW;
    }

    public int getGridCellH() {
        return gridCellH;
    }

    public void setGridCellH(int gridCellH) {
        this.gridCellH = gridCellH;
    }

    public int getGridCellW() {
        return gridCellW;
    }

    public void setGridCellW(int gridCellW) {
        this.gridCellW = gridCellW;
    }
}
