package com.azermehdibeyli.www.a4daysschedule;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Azer on 20.11.2017.
 */

public class SettingsActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private Date[] shiftDates;

    private EditText etFirstWorkingDate;
    private Spinner spinShiftNames;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initializeObjects();
        spinShiftNames.setSelection(getShiftNoFromSettings(), true);
        setListeners();
        updateViewOnSeekBarProgressChanged(spinShiftNames.getSelectedItemPosition());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeObjects() {
        shiftDates = getFirstWorkingDatesFromSettings();
        etFirstWorkingDate = (EditText) findViewById(R.id.settings_et_firstWorkingDate);
        etFirstWorkingDate.setInputType(InputType.TYPE_NULL);

        spinShiftNames = (Spinner) findViewById(R.id.settings_sp_shiftNames);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.shifts_array, R.layout.shift_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinShiftNames.setAdapter(adapter);
    }

    private Date[] getFirstWorkingDatesFromSettings() {
        Date[] dates = new Date[4];

        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preferenceFileName), Context.MODE_PRIVATE);

        for (int i = 0; i < dates.length; i++) {
            Calendar cal = Calendar.getInstance();
            try {
                String dateStringFromPrefence = sharedPref.getString(getString(R.string.scheduleStartDate) + (i), null);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dates[i] = sdf.parse(dateStringFromPrefence);
            } catch (Exception e) {
                //cal.set(1901, 0, 1);
                dates[i] = null;
            }
        }

        return dates;
    }

    private int getShiftNoFromSettings() {
        int ret = 0;

        Context context = getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.preferenceFileName), Context.MODE_PRIVATE);

        try {
            ret = sharedPref.getInt(getString(R.string.scheduleShiftNo), 0);
        } catch (Exception e) {
        }

        return ret;
    }

    private void setListeners() {
        spinShiftNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateViewOnSeekBarProgressChanged(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        etFirstWorkingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });
    }

    private void updateViewOnSeekBarProgressChanged(int seekBarProgress) {
        if (shiftDates[seekBarProgress] != null)
            etFirstWorkingDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(shiftDates[seekBarProgress]));
        else
            etFirstWorkingDate.setText("");

    }

    public void savePreferencesAndFinish(View view) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preferenceFileName), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (int i = 0; i < shiftDates.length; i++) {
            if (shiftDates[i] != null) {
                String dateStr = sdf.format(shiftDates[i]);
                editor.putString(getString(R.string.scheduleStartDate) + i, dateStr);
            } else {
                Calendar cal = Calendar.getInstance();
                String dateStr = sdf.format(cal.getTime());
                editor.putString(getString(R.string.scheduleStartDate) + i, dateStr);
            }
        }
        editor.putInt(getString(R.string.scheduleShiftNo), spinShiftNames.getSelectedItemPosition());
        editor.commit();

        finish();
    }

    public void showDatePickerDialog(View view) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "date");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, month, day);
        shiftDates[spinShiftNames.getSelectedItemPosition()] = cal.getTime();
        updateViewOnSeekBarProgressChanged(spinShiftNames.getSelectedItemPosition());
    }

    public static class DatePickerFragment extends android.support.v4.app.DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(),
                    (DatePickerDialog.OnDateSetListener)
                            getActivity(), year, month, day);
        }
    }
}
