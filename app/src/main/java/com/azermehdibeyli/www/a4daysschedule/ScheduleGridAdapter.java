package com.azermehdibeyli.www.a4daysschedule;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Azer on 26.11.2017.
 */

public class ScheduleGridAdapter extends BaseAdapter {

    private Context mContext;
    private final DayOfSchedule[] daysOfSchedule;
    private int cellImageWidth;

    public ScheduleGridAdapter(Context mContext, DayOfSchedule[] daysOfSchedule, int cellImageWidth) {
        this.mContext = mContext;
        this.daysOfSchedule = daysOfSchedule;
        this.cellImageWidth = cellImageWidth;
    }

    public class ViewHolder {
        TextView day;
        ImageView dayTypeImage;
        RelativeLayout dayTypeImageParent;
    }

    @Override
    public int getCount() {
        return daysOfSchedule.length;
    }

    @Override
    public Object getItem(int position) {
        return daysOfSchedule[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.schedule_grid_cell, null);
            holder = new ViewHolder();
            holder.day = (TextView) convertView.findViewById(R.id.schedule_grid_cell_day);
            holder.dayTypeImage = (ImageView) convertView.findViewById(R.id.schedule_grid_cell_image);
            holder.dayTypeImageParent = (RelativeLayout) convertView.findViewById(R.id.schedule_grid_cell_image_parent);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.day.setText(daysOfSchedule[position].getDay() + "");
        holder.day.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 2.12f));

        switch (daysOfSchedule[position].getDayType()) {
            case DEFAULT:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.day_default);
                holder.day.setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                break;
            case DAY_SHIFT:
                holder.dayTypeImage.setBackgroundResource(R.mipmap.day_shift);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.day_shift);
                break;
            case DAY_SHIFT_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.day_shift_holiday);
                break;
            case DAY_NIGHT:
                holder.dayTypeImage.setBackgroundResource(R.mipmap.night_shift);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.night_shift);
                break;
            case DAY_NIGHT_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.night_shift_holiday);
                break;
            case DAY_OFF_1:
                holder.dayTypeImage.setBackgroundResource(R.mipmap.dayoff_1);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1);
                break;
            case DAY_OFF_1_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1_holiday);
                break;
            case DAY_OFF_2:
                holder.dayTypeImage.setBackgroundResource(R.mipmap.dayoff_2);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1);
                break;
            case DAY_OFF_2_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1_holiday);
                break;
            case TODAY_DAY_SHIFT:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.day_shift_today);
                break;
            case TODAY_DAY_SHIFT_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.day_shift_holiday_today);
                break;
            case TODAY_DAY_NIGHT:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.night_shift_today);
                break;
            case TODAY_DAY_NIGHT_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.night_shift_holiday_today);
                break;
            case TODAY_DAY_OFF_1:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1_today);
                break;
            case TODAY_DAY_OFF_1_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1_holiday_today);
                break;
            case TODAY_DAY_OFF_2:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1_today);
                break;
            case TODAY_DAY_OFF_2_HOLIDAY:
                holder.dayTypeImage.setVisibility(View.INVISIBLE);
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.dayoff_1_holiday_today);
                break;
        }
        RelativeLayout.LayoutParams dayTypeImageParams = new RelativeLayout.LayoutParams(cellImageWidth, cellImageWidth);
        holder.dayTypeImage.setLayoutParams(dayTypeImageParams);

        return convertView;
    }
}
