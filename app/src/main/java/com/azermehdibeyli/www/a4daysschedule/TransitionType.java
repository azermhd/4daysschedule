package com.azermehdibeyli.www.a4daysschedule;

/**
 * Created by azer.mehdibeyli on 28.12.2017.
 */

public enum TransitionType {
    NEXT_MONTH,
    PREVIOUS_MONTH,
    NEXT_SHIFT,
    PREVIOUS_SHIFT
}
