package com.azermehdibeyli.www.a4daysschedule;

/**
 * Created by Azer on 26.11.2017.
 */

public enum DayType {
    DAY_OFF_1,
    DAY_OFF_1_HOLIDAY,
    DAY_OFF_2,
    DAY_OFF_2_HOLIDAY,
    DAY_SHIFT,
    DAY_SHIFT_HOLIDAY,
    DAY_NIGHT,
    DAY_NIGHT_HOLIDAY,
    DEFAULT,
    TODAY_DAY_OFF_1,
    TODAY_DAY_OFF_1_HOLIDAY,
    TODAY_DAY_OFF_2,
    TODAY_DAY_OFF_2_HOLIDAY,
    TODAY_DAY_SHIFT,
    TODAY_DAY_SHIFT_HOLIDAY,
    TODAY_DAY_NIGHT,
    TODAY_DAY_NIGHT_HOLIDAY,
}
