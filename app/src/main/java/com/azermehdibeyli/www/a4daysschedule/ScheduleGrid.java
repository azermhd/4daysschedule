package com.azermehdibeyli.www.a4daysschedule;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by azer.mehdibeyli on 27.11.2017.
 */

public class ScheduleGrid {

    Settings settings;
    private ArrayList<Date> nonWorkingDateList;

    public ScheduleGrid(Context context) {
        settings = new Settings(context);
        nonWorkingDateList = getNonWorkingDayList();
    }

    public DayOfSchedule[] getDaysOfSchedule(Date dateSelectedMonth, int shiftNo) {
        DayOfSchedule[] daysOfSchedule;
        Date dateFromSettings = settings.getFirstWorkingDateFromSettings(shiftNo);
        int scheduleNoInFirstDayOfMonth = 0;
        int dayOfWeekOfFirstDayInMonth = 0;
        int numOfDaysInMonth = 0;
        int numOfDaysInPreviousMonth = 0;
        int dayOfWeekOfLastDayInMonth = 0;

        GregorianCalendar calendarFromSettings = new GregorianCalendar();
        calendarFromSettings.setTime(dateFromSettings);

        GregorianCalendar calendarSelectedMonth = new GregorianCalendar();
        calendarSelectedMonth.setTime(dateSelectedMonth);
        calendarSelectedMonth.set(calendarSelectedMonth.get(Calendar.YEAR), calendarSelectedMonth.get(Calendar.MONTH), 1, 0, 0, 0);
        calendarSelectedMonth.set(Calendar.MILLISECOND, 0);
        dateSelectedMonth = calendarSelectedMonth.getTime();

        GregorianCalendar calendarPreviousMonth = new GregorianCalendar();
        calendarPreviousMonth.setTime(calendarSelectedMonth.getTime());
        calendarPreviousMonth.add(Calendar.MONTH, -1);
        numOfDaysInPreviousMonth = calendarPreviousMonth.getActualMaximum(Calendar.DAY_OF_MONTH);

        dayOfWeekOfFirstDayInMonth = calendarSelectedMonth.get(Calendar.DAY_OF_WEEK);
        dayOfWeekOfFirstDayInMonth = convertDayOfWeekToMondayFirst(dayOfWeekOfFirstDayInMonth);
        numOfDaysInMonth = calendarSelectedMonth.getActualMaximum(Calendar.DAY_OF_MONTH);

        GregorianCalendar calendarLastDayOfSelectedMonth = new GregorianCalendar();
        calendarLastDayOfSelectedMonth.setTime(dateSelectedMonth);
        calendarLastDayOfSelectedMonth.add(Calendar.DAY_OF_MONTH, numOfDaysInMonth - 1);
        Date dateLastDayOfSelectedMonth = calendarLastDayOfSelectedMonth.getTime();

        dayOfWeekOfLastDayInMonth = calendarLastDayOfSelectedMonth.get(Calendar.DAY_OF_WEEK);
        dayOfWeekOfLastDayInMonth = convertDayOfWeekToMondayFirst(dayOfWeekOfLastDayInMonth);

        ArrayList<DayOfSchedule> daysOfScheduleList = new ArrayList<>();

        long daysBetweenSelectedAndSettings = TimeUnit.DAYS.convert(
                dateFromSettings.getTime() - dateSelectedMonth.getTime(),
                TimeUnit.MILLISECONDS);

        if (daysBetweenSelectedAndSettings < 0) {
            scheduleNoInFirstDayOfMonth = (int) (-daysBetweenSelectedAndSettings + (daysBetweenSelectedAndSettings / 4) * 4);
        } else if (daysBetweenSelectedAndSettings > 0) {
            scheduleNoInFirstDayOfMonth = 4 - (int) (daysBetweenSelectedAndSettings - (daysBetweenSelectedAndSettings / 4) * 4);
        }

        for (int i = 0; i < dayOfWeekOfFirstDayInMonth - 1; i++) {
            daysOfScheduleList.add(new DayOfSchedule(DayType.DEFAULT, numOfDaysInPreviousMonth - dayOfWeekOfFirstDayInMonth + 2 + i));
        }

        int scheduleCounter = scheduleNoInFirstDayOfMonth;
        if (scheduleCounter >= 4)
            scheduleCounter = 0;

        for (int i = 0; i < numOfDaysInMonth; i++) {
            switch (scheduleCounter) {
                case 0:
                    daysOfScheduleList.add(new DayOfSchedule(DayType.DAY_SHIFT, i + 1));
                    break;
                case 1:
                    daysOfScheduleList.add(new DayOfSchedule(DayType.DAY_NIGHT, i + 1));
                    break;
                case 2:
                    daysOfScheduleList.add(new DayOfSchedule(DayType.DAY_OFF_1, i + 1));
                    break;
                case 3:
                    daysOfScheduleList.add(new DayOfSchedule(DayType.DAY_OFF_2, i + 1));
                    break;
                default:
                    daysOfScheduleList.add(new DayOfSchedule(DayType.DEFAULT, 0));
            }
            scheduleCounter++;

            if (scheduleCounter >= 4)
                scheduleCounter = 0;
        }

        for (int i = 0; i < 7 - dayOfWeekOfLastDayInMonth; i++) {
            daysOfScheduleList.add(new DayOfSchedule(DayType.DEFAULT, i + 1));
        }

        daysOfSchedule = daysOfScheduleList.toArray(new DayOfSchedule[0]);

        ArrayList<Calendar> nonWorkingDayListInThisMonth = new ArrayList<>();

        int month = calendarSelectedMonth.get(Calendar.MONTH);
        int year = calendarSelectedMonth.get(Calendar.YEAR);
        for (Date date : nonWorkingDateList) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (calendar.get(Calendar.MONTH) == month && calendar.get(Calendar.YEAR) == year) {
                nonWorkingDayListInThisMonth.add(calendar);
            }
        }

        for (DayOfSchedule dayOfSchedule : daysOfSchedule) {
            if (dayOfSchedule.getDay() != 0) {
                for (Calendar date : nonWorkingDayListInThisMonth) {
                    if (date.get(Calendar.DAY_OF_MONTH) == dayOfSchedule.getDay()) {
                        DayType dayType = dayOfSchedule.getDayType();
                        switch (dayType) {
                            case DAY_SHIFT:
                                dayOfSchedule.setDayType(DayType.DAY_SHIFT_HOLIDAY);
                                break;
                            case DAY_NIGHT:
                                dayOfSchedule.setDayType(DayType.DAY_NIGHT_HOLIDAY);
                                break;
                            case DAY_OFF_1:
                                dayOfSchedule.setDayType(DayType.DAY_OFF_1_HOLIDAY);
                                break;
                            case DAY_OFF_2:
                                dayOfSchedule.setDayType(DayType.DAY_OFF_2_HOLIDAY);
                                break;
                        }
                    }
                }
            }
        }

        Calendar calendarCurrentMonth = Calendar.getInstance();
        int dayOfCurrentMonth = calendarCurrentMonth.get(Calendar.DAY_OF_MONTH);
        if (calendarCurrentMonth.get(Calendar.MONTH) == calendarSelectedMonth.get(Calendar.MONTH) &&
                calendarCurrentMonth.get(Calendar.YEAR) == calendarSelectedMonth.get(Calendar.YEAR)) {
            for (int i = dayOfWeekOfFirstDayInMonth - 1; i < daysOfSchedule.length; i++) {
                DayOfSchedule dayOfSchedule = daysOfSchedule[i];
                if (dayOfSchedule.getDay() == dayOfCurrentMonth) {
                    DayType dayType = dayOfSchedule.getDayType();
                    switch (dayType) {
                        case DAY_SHIFT:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_SHIFT);
                            break;
                        case DAY_NIGHT:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_NIGHT);
                            break;
                        case DAY_OFF_1:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_OFF_1);
                            break;
                        case DAY_OFF_2:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_OFF_2);
                            break;
                        case DAY_SHIFT_HOLIDAY:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_SHIFT_HOLIDAY);
                            break;
                        case DAY_NIGHT_HOLIDAY:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_NIGHT_HOLIDAY);
                            break;
                        case DAY_OFF_1_HOLIDAY:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_OFF_1_HOLIDAY);
                            break;
                        case DAY_OFF_2_HOLIDAY:
                            dayOfSchedule.setDayType(DayType.TODAY_DAY_OFF_2_HOLIDAY);
                            break;
                    }
                    break;
                }
            }
        }

        return daysOfSchedule;
    }

    public int convertDayOfWeekToMondayFirst(int dayOfWeek) {
        dayOfWeek--;
        if (dayOfWeek == 0)
            dayOfWeek += 7;
        return dayOfWeek;
    }

    private ArrayList<Date> getNonWorkingDayList() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<Date> dateList = new ArrayList<>();
        try {
            dateList.add(sdf.parse("2017-01-01"));
            dateList.add(sdf.parse("2017-01-02"));
            dateList.add(sdf.parse("2017-01-20"));

            dateList.add(sdf.parse("2017-03-08"));
            dateList.add(sdf.parse("2017-03-20"));
            dateList.add(sdf.parse("2017-03-21"));
            dateList.add(sdf.parse("2017-03-22"));
            dateList.add(sdf.parse("2017-03-23"));
            dateList.add(sdf.parse("2017-03-24"));

            dateList.add(sdf.parse("2017-05-09"));
            dateList.add(sdf.parse("2017-05-28"));

            dateList.add(sdf.parse("2017-06-15"));
            dateList.add(sdf.parse("2017-06-26"));

            dateList.add(sdf.parse("2017-11-09"));

            dateList.add(sdf.parse("2017-12-31"));

            dateList.add(sdf.parse("2017-06-27"));
            dateList.add(sdf.parse("2017-09-01"));
            dateList.add(sdf.parse("2017-09-02"));

            ////////*********************************
            dateList.add(sdf.parse("2018-01-01"));
            dateList.add(sdf.parse("2018-01-02"));
            dateList.add(sdf.parse("2018-01-20"));

            dateList.add(sdf.parse("2018-03-08"));
            dateList.add(sdf.parse("2018-03-20"));
            dateList.add(sdf.parse("2018-03-21"));
            dateList.add(sdf.parse("2018-03-22"));
            dateList.add(sdf.parse("2018-03-23"));
            dateList.add(sdf.parse("2018-03-24"));

            dateList.add(sdf.parse("2018-05-09"));
            dateList.add(sdf.parse("2018-05-28"));

            dateList.add(sdf.parse("2018-06-15"));
            dateList.add(sdf.parse("2018-06-26"));

            dateList.add(sdf.parse("2018-11-09"));

            dateList.add(sdf.parse("2018-12-31"));

            dateList.add(sdf.parse("2018-06-15"));
            dateList.add(sdf.parse("2018-06-16"));
            dateList.add(sdf.parse("2018-08-22"));
            dateList.add(sdf.parse("2018-08-23"));
            dateList.add(sdf.parse("2018-04-11"));

            ////////*********************************
            dateList.add(sdf.parse("2019-01-01"));
            dateList.add(sdf.parse("2019-01-02"));
            dateList.add(sdf.parse("2019-01-20"));

            dateList.add(sdf.parse("2019-03-08"));
            dateList.add(sdf.parse("2019-03-20"));
            dateList.add(sdf.parse("2019-03-21"));
            dateList.add(sdf.parse("2019-03-22"));
            dateList.add(sdf.parse("2019-03-23"));
            dateList.add(sdf.parse("2019-03-24"));

            dateList.add(sdf.parse("2019-05-09"));
            dateList.add(sdf.parse("2019-05-28"));

            dateList.add(sdf.parse("2019-06-15"));
            dateList.add(sdf.parse("2019-06-26"));

            dateList.add(sdf.parse("2019-11-09"));

            dateList.add(sdf.parse("2019-12-31"));

            dateList.add(sdf.parse("2019-12-27"));

            dateList.add(sdf.parse("2019-06-05"));
            dateList.add(sdf.parse("2019-06-06"));
            dateList.add(sdf.parse("2019-08-12"));
            dateList.add(sdf.parse("2019-08-13"));

            //nonWorkingDateList = dateList;

        } catch (Exception ex) {
        }
        return dateList;
    }
}
