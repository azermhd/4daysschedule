package com.azermehdibeyli.www.a4daysschedule;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private GridView gridView;
    private TextView tvGridHeader;
    private TextView tvShiftName;
    private LinearLayout weekNumbersLayout;
    private RelativeLayout mainParentLayout;
    private RelativeLayout swipeTrainingLayout;

    private Settings settings;
    private ViewSettings viewSettings;
    private ScheduleGrid scheduleGridMethods;

    private Date selectedMonth;
    private int selectedShift;
    private DateFormat dateFormatForGridHeader;

    private GestureDetectorCompat detector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setDrawerAndNavigationLayout();
        initializeObjects();
        setListeners();
        adjustViewDimens();
        //addSettingAdjustNeededText();
    }

    @Override
    protected void onResume() {
        Log.i("onResume", "onResume");
        setAdapterToScheduleGrid(selectedMonth, selectedShift);
        tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
        setTextTvShiftName();
        addNotificationsIfNeeded();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initializeObjects() {
        gridView = (GridView) findViewById(R.id.main_gridView);
        tvGridHeader = (TextView) findViewById(R.id.main_gridTitle);
        tvShiftName = (TextView) findViewById(R.id.main_shiftName);

        weekNumbersLayout = (LinearLayout) findViewById(R.id.main_weekNumbersLayout);
        mainParentLayout = findViewById(R.id.main_mainParentLayout);

        settings = new Settings(getApplicationContext());
        viewSettings = new ViewSettings(this);
        scheduleGridMethods = new ScheduleGrid(getApplicationContext());
        dateFormatForGridHeader = new SimpleDateFormat("MMMM yyyy");

        detector = new GestureDetectorCompat(this, new MyGridGestureListener());

        selectedShift = settings.getShiftNoFromSettings();
        setSelectedShift(selectedShift);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        selectedMonth = calendar.getTime();

        setAdapterToScheduleGrid(selectedMonth, selectedShift);

        tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
        setTextTvShiftName();
    }

    private void setTextTvShiftName() {
        tvShiftName.setText("Group " + (selectedShift + 1));
    }

    private void adjustViewDimens() {
        int cellImageWidth = viewSettings.getGridCellW();
        tvShiftName.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 2.50f));
        tvGridHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 1.89f));

        final int childCount = weekNumbersLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            TextView textView = (TextView) weekNumbersLayout.getChildAt(i);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 2.80f));
        }
    }

    public void nextMonth() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(selectedMonth);
        cal.add(Calendar.MONTH, 1);
        selectedMonth = cal.getTime();
        setAdapterToScheduleGrid(selectedMonth, selectedShift);
        tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
    }

    public void previousMonth() {
        Calendar cal = new GregorianCalendar();
        cal.setTime(selectedMonth);
        cal.add(Calendar.MONTH, -1);
        selectedMonth = cal.getTime();
        setAdapterToScheduleGrid(selectedMonth, selectedShift);
        tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
    }

    public void previousShift() {
        setSelectedShift(selectedShift - 1);
        setAdapterToScheduleGrid(selectedMonth, selectedShift);
        tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
        setTextTvShiftName();
    }

    public void nextShift() {
        setSelectedShift(selectedShift + 1);
        setAdapterToScheduleGrid(selectedMonth, selectedShift);
        tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
        setTextTvShiftName();
    }

    private void setListeners() {
        gridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                detector.onTouchEvent(event);
                return false;
            }
        });

        //gridView.setOnTouchListener(new MyTouchListener());
    }

    private void setAdapterToScheduleGrid(Date dateSelectedMonth, int shiftNo) {
        ScheduleGridAdapter adapter = new ScheduleGridAdapter(MainActivity.this,
                scheduleGridMethods.getDaysOfSchedule(dateSelectedMonth, shiftNo),
                viewSettings.getGridCellW());
        gridView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setDrawerAndNavigationLayout() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.menu_settings:
                        Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(i);
                        break;
                    case R.id.menu_about:
                        Intent a = new Intent(MainActivity.this, AboutActivity.class);
                        startActivity(a);
                        break;
                }
                return false;
            }
        });
    }

    public void setSelectedShift(int selectedShift) {
        if (selectedShift < 0)
            this.selectedShift = 0;
        else if (selectedShift > 3)
            this.selectedShift = 3;
        else
            this.selectedShift = selectedShift;
    }

    private void addNotificationsIfNeeded() {
        boolean swipeTrainerNeeded = addSwipeTrainerIfNeeded();
        if (!swipeTrainerNeeded) {
            addSettingAdjustNeededText();
        }
    }

    private boolean addSwipeTrainerIfNeeded() {
        swipeTrainingLayout = findViewById(R.id.main_swipeTrainingLayout);
        if (swipeTrainingLayout != null)
            mainParentLayout.removeView(swipeTrainingLayout);

        if (!settings.isSwipeTrained() && !settings.isAllDatesAdjusted()) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int tenDp = Math.round(displayMetrics.density * 10);

            swipeTrainingLayout = new RelativeLayout(this);
            swipeTrainingLayout.setId(R.id.main_swipeTrainingLayout);
            swipeTrainingLayout.setBackgroundResource(R.drawable.white_rounded_backgroud_alpha);
            swipeTrainingLayout.setPadding(tenDp, tenDp, tenDp, tenDp);
            RelativeLayout.LayoutParams swipeTrainingLayoutLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            swipeTrainingLayout.setLayoutParams(swipeTrainingLayoutLayoutParams);

            ImageView swipeImageView = new ImageView(this);
            swipeImageView.setBackgroundResource(R.mipmap.swipe_arrows);
            swipeImageView.setScaleType(ImageView.ScaleType.CENTER);
            swipeImageView.setId(R.id.main_swipeTrainingImage);

            RelativeLayout.LayoutParams swipeImageViewLayoutParams = new RelativeLayout.LayoutParams((int) (displayMetrics.widthPixels / 2.3), (int) (displayMetrics.widthPixels / 2.3));
            swipeImageViewLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            swipeImageViewLayoutParams.setMargins(0, (int) (displayMetrics.heightPixels / 5.9), 0, (int) (30 * displayMetrics.density));
            swipeImageView.setLayoutParams(swipeImageViewLayoutParams);

            TextView swipeTrainingTextView = new TextView(getApplicationContext());
            swipeTrainingTextView.setId(R.id.main_swipeTrainingText);
            swipeTrainingTextView.setText(this.getString(R.string.mainSwipeTrainingTextText));
            swipeTrainingTextView.setTextColor(getResources().getColor(R.color.mainBackColorDark));
            swipeTrainingTextView.setBackgroundResource(R.drawable.white_rounded_backgroud);
            swipeTrainingTextView.setPadding(tenDp, tenDp, tenDp, tenDp);
            swipeTrainingTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, displayMetrics.widthPixels / 25);
            swipeTrainingTextView.setText(R.string.mainSwipeTrainingTextText);

            RelativeLayout.LayoutParams swipeTrainingTextViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            swipeTrainingTextViewLayoutParams.addRule(RelativeLayout.BELOW, R.id.main_swipeTrainingImage);
            swipeTrainingTextViewLayoutParams.setMargins(0, 0, 0, tenDp);
            swipeTrainingTextView.setLayoutParams(swipeTrainingTextViewLayoutParams);

            Button swipeTrainingButton = new Button(this);
            swipeTrainingButton.setId(R.id.main_swipeTrainingButton);
            swipeTrainingButton.setBackgroundResource(R.drawable.save_button_background);
            swipeTrainingButton.setText("GOT IT");
            swipeTrainingButton.setTextColor(getResources().getColor(R.color.white));

            RelativeLayout.LayoutParams swipeTrainingButtonLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            swipeTrainingButtonLayoutParams.addRule(RelativeLayout.BELOW, R.id.main_swipeTrainingText);
            swipeTrainingButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            swipeTrainingButton.setLayoutParams(swipeTrainingButtonLayoutParams);

            swipeTrainingLayout.addView(swipeImageView);
            swipeTrainingLayout.addView(swipeTrainingTextView);
            swipeTrainingLayout.addView(swipeTrainingButton);

            mainParentLayout.addView(swipeTrainingLayout);

            swipeTrainingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainParentLayout.removeView(swipeTrainingLayout);
                    settings.setSwipeTrained(true);
                    addNotificationsIfNeeded();
                }
            });
            return true;
        } else if (!settings.isSwipeTrained() && settings.isAllDatesAdjusted()) {
            settings.setSwipeTrained(true);
        }

        return false;
    }

    private void addSettingAdjustNeededText() {
        LinearLayout datesNeedAdjustedLayout = findViewById(R.id.main_datesNeedAdjustedLayout);
        if (datesNeedAdjustedLayout != null)
            mainParentLayout.removeView(datesNeedAdjustedLayout);

        if (!settings.isAllDatesAdjusted()) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            float density = displayMetrics.density;
            int tenDp = Math.round(density * 10);
            int fiveDp = Math.round(density * 5);
            int fourDp = Math.round(density * 4);

            LinearLayout linearLayout = new LinearLayout(getApplicationContext());
            RelativeLayout.LayoutParams linearLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayoutParams.setMargins(tenDp, 0, tenDp, tenDp);
            linearLayoutParams.addRule(RelativeLayout.BELOW, R.id.main_gridParentLayout);
            linearLayout.setLayoutParams(linearLayoutParams);
            linearLayout.setBackgroundResource(R.drawable.day_default);
            linearLayout.setPadding(fourDp, fourDp - (int) (3 * density), fourDp, fourDp);
            linearLayout.setId(R.id.main_datesNeedAdjustedLayout);

            TextView textView = new TextView(getApplicationContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            textView.setLayoutParams(layoutParams);
            textView.setText(this.getString(R.string.mainAdjustDate));
            textView.setTextColor(getResources().getColor(R.color.mainBackColorDark));

            linearLayout.addView(textView);
            mainParentLayout.addView(linearLayout);
        }
    }

    private class MyGridGestureListener extends CustomGridViewGestureListener {
        public void onSwipeLeft() {
            nextMonth();
        }

        public void onSwipeRight() {
            previousMonth();
        }

        public void onSwipeTop() {
            nextShift();
        }

        public void onSwipeBottom() {
            previousShift();
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        startAnimation(TransitionType.PREVIOUS_SHIFT);
//                    }
//                });
//            }
        }
    }

//    private void startAnimation(TransitionType transitionType) {
//
//        Display display = this.getWindowManager().getDefaultDisplay();
//        Point displaySize = new Point();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            display.getRealSize(displaySize);
//        } else {
//            display.getSize(displaySize);
//        }
//
//        TranslateAnimation animation;
//        switch (transitionType) {
//            case NEXT_SHIFT:
//                if (isMainGridVisible) {
//                    animation = new TranslateAnimation(0, 0, 0, viewSettings.getScreenY());
//                    animation.setFillAfter(true);
//                    animation.setDuration(250);
//                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
//                    gridView.startAnimation(animation);
//
//                    animation = new TranslateAnimation(0, 0, 0, viewSettings.getScreenY());
//                    animation.setFillAfter(true);
//                    animation.setDuration(250);
//                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
//                    gridViewSecond.startAnimation(animation);
//
//                } else {
//                    animation = new TranslateAnimation((int) (displaySize.x * 1.10), 0, 0, 0);
//                }
//
//                break;
//        }
//
//
//    }

//    private int getTextWidth(TextView textView) {
//        Rect bounds = new Rect();
//        Paint textPaint = textView.getPaint();
//        textPaint.getTextBounds(textView.getText().toString(), 0, textView.getText().length(), bounds);
//        return bounds.width();
//    }

/*

    private boolean isHorizontal;
    private boolean isDirectionSet = false;
    private int directionSetLimit;
    private int xx;
    private int yy;
    private int leftMargin;
    private int topMargin;
    private int rightMargin;

    private class MyTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent event) {

            final int x = (int) event.getRawX();
            final int y = (int) event.getRawY();
            RelativeLayout.LayoutParams layoutParams;
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    leftMargin = layoutParams.leftMargin;
                    topMargin = layoutParams.topMargin;
                    rightMargin = layoutParams.rightMargin;
                    xx = x;
                    yy = y;
                    break;
                case MotionEvent.ACTION_MOVE:
                    layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

                    if (isDirectionSet) {
                        if (isHorizontal) {
                            layoutParams.leftMargin = leftMargin + x - xx;
                            //layoutParams.topMargin = topMargin;
                            layoutParams.rightMargin = rightMargin + xx - x;
                        } else {
                            //layoutParams.leftMargin = leftMargin;
                            layoutParams.topMargin = topMargin + y - yy;
                            //layoutParams.rightMargin = rightMargin;
                        }
                    } else {
                        Log.i("XX", "" + xx);

                        if (Math.abs(x - xx) > 5 || (Math.abs(y - yy) > 5)) {
                            if (Math.abs(x - xx) > (Math.abs(y - yy))) {
                                isHorizontal = true;
                                isDirectionSet = true;
                            } else {
                                isHorizontal = false;
                                isDirectionSet = true;
                            }
                        }
                    }

                    view.setLayoutParams(layoutParams);
                    break;
                case MotionEvent.ACTION_UP:
                    Log.i("ACTION_UP", "ACTION_UP");
                    layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

                    layoutParams.leftMargin = 0;
                    layoutParams.topMargin = (int) (34 * getResources().getDisplayMetrics().density);
                    layoutParams.rightMargin = 0;
                    //isDirectionSet = false;
                    break;
            }

            mainParentLayout.invalidate();
            return true;
        }
    }
*/
}
